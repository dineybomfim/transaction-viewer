/*
 *	TransactionsLO.swift
 *	Transaction Viewer
 *
 *	Created by Diney on 27/01/17.
 *	Copyright 2017 Badoo. All rights reserved.
 */

import UIKit

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

public class TransactionsLO {

	public class Group {
		public var sku: String = ""
		
		public lazy var transactions: [TransactionVO] = {
			return TransactionsLO.instance.all.filter { $0.sku == self.sku }
		}()
		
		init(sku: String) {
			self.sku = sku
		}
	}
	
//**************************************************
// MARK: - Properties
//**************************************************
	
	public lazy var all: [TransactionVO] = {
		
		if let raw = Bundle.loadPlist(named: "transactions") {
			return raw.map { TransactionVO($0) }
		}
		
		return []
	}()
	
	public lazy var groups: [Group] = {
		let transactions = self.all
		var skuSet = Set<String>()
		
		transactions.forEach { skuSet.insert($0.sku) }
		
		return skuSet.map { Group(sku: $0) }
	}()
	
	public var currentGroup: Group?
	
	static public let instance: TransactionsLO = TransactionsLO()

//**************************************************
// MARK: - Constructors
//**************************************************

//**************************************************
// MARK: - Protected Methods
//**************************************************

//**************************************************
// MARK: - Exposed Methods
//**************************************************

//**************************************************
// MARK: - Overridden Methods
//**************************************************
}
