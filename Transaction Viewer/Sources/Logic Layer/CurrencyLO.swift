/*
 *	CurrencyLO.swift
 *	Transaction Viewer
 *
 *	Created by Diney on 27/01/17.
 *	Copyright 2017 Badoo. All rights reserved.
 */

import UIKit

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

public class CurrencyLO {

//**************************************************
// MARK: - Properties
//**************************************************

	public lazy var all: [RateVO] = {
		
		if let raw = Bundle.loadPlist(named: "rates") {
			return raw.map { RateVO($0) }
		}
		
		return []
	}()
	
	static public let instance: CurrencyLO = CurrencyLO()
	
//**************************************************
// MARK: - Constructors
//**************************************************

//**************************************************
// MARK: - Protected Methods
//**************************************************

//**************************************************
// MARK: - Exposed Methods
//**************************************************

	public func localized(for amount: Double, code: String) -> String {
		let number = NSNumber(floatLiteral: amount)
		let formatter = NumberFormatter()
		
		formatter.currencyCode = code
		formatter.numberStyle = .currency
		
		if let string = formatter.string(from: number) {
			return string
		}
		
		return ""
	}
	
	public func convertCurrency(from: String, to: String, amount: Double) -> Double? {
		
		// Avoid current currency convertion
		if from == to {
			return amount
		}
		
		let possibleInputs = self.all.filter { $0.from == from }
		
		if let converter = possibleInputs.filter({ $0.to == to }).first {
			return amount * converter.rate
		} else {
			// Recursive function to find the best convertion
			for input in possibleInputs {
				if let result = self.convertCurrency(from: input.to, to: to, amount: amount * input.rate) {
					return result
				}
			}
		}
		
		return nil
	}
	
//**************************************************
// MARK: - Overridden Methods
//**************************************************

}
