/*
*	TransactionsVC.swift
*	Transaction Viewer
*
*	Created by Diney on 27/01/17.
*	Copyright 2017 Badoo. All rights reserved.
*/

import UIKit

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class TransactionsVC : UITableViewController {
	
//**************************************************
// MARK: - Overridden Methods
//**************************************************

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let sku = TransactionsLO.instance.currentGroup?.sku {
			self.title = "Transactions for \(sku)"
		} else {
			self.title = "Select a transaction to show"
		}
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return TransactionsLO.instance.currentGroup?.transactions.count ?? 0
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		let logic = CurrencyLO.instance
		
		if let transaction = TransactionsLO.instance.currentGroup?.transactions[indexPath.row] {
			cell.textLabel?.text = logic.localized(for: transaction.amount, code: transaction.currency)
			
			if let value = logic.convertCurrency(from: transaction.currency,
			                                     to: "GBP",
			                                     amount: transaction.amount) {
				cell.detailTextLabel?.text = logic.localized(for: value, code: "GBP")
			} else {
				cell.detailTextLabel?.text = "There is no convertion to GBP"
			}
		}
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		
		let logic = CurrencyLO.instance
		var sum = 0.0
		
		TransactionsLO.instance.currentGroup?.transactions.forEach {
			if let value = logic.convertCurrency(from: $0.currency, to: "GBP", amount: $0.amount) {
				sum += value
			}
		}
		
		return "Total: \(logic.localized(for: sum, code: "GBP"))"
	}
}

