/*
*	ProductsVC.swift
*	Transaction Viewer
*
*	Created by Diney on 27/01/17.
*	Copyright 2017 Badoo. All rights reserved.
*/

import UIKit

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class ProductsVC : UITableViewController {

//**************************************************
// MARK: - Overridden Methods
//**************************************************

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return TransactionsLO.instance.groups.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let group = TransactionsLO.instance.groups[indexPath.row]
		cell.textLabel?.text = group.sku
		cell.detailTextLabel?.text = "\(group.transactions.count) transactions"
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let logic = TransactionsLO.instance
		let group = logic.groups[indexPath.row]
		logic.currentGroup = group
	}
}

