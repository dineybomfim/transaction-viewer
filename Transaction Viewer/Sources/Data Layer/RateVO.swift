/*
 *	RateVO.swift
 *	Transaction Viewer
 *
 *	Created by Diney on 27/01/17.
 *	Copyright 2017 Badoo. All rights reserved.
 */

import UIKit

//**********************************************************************************************************
//
// MARK: - Type -
//
//**********************************************************************************************************

public struct RateVO  {

	public var from: String = ""
	public var to: String = ""
	public var rate: Double = 0.0
}

//**********************************************************************************************************
//
// MARK: - Extension -
//
//**********************************************************************************************************

extension RateVO {
	public init(_ raw: [String:Any]) {
		self.from = raw["from"] as? String ?? ""
		self.to = raw["to"] as? String ?? ""
		self.rate = Double(raw["rate"] as? String ?? "") ?? 0.0
	}
}
