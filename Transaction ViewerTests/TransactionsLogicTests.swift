/*
*	TransactionsLogicTests.swift
*	Transaction Viewer
*
*	Created by Diney on 27/01/17.
*	Copyright 2017 Badoo. All rights reserved.
*/

import XCTest
@testable import Transaction_Viewer

class TransactionsLogicTests : XCTestCase {
    
    func testTransactionsLogic_WithLoadingAllPlistContent_ShouldLoadMoreThan0() {
		let transaction = TransactionsLO.instance.all
		
		XCTAssert(transaction.count > 0, "You're testing an empty transaction file")
    }
	
	func testTransactionsLogic_Withgroups_ShouldResultInGrouppedValues() {
		let transaction = TransactionsLO.instance.all
		let groups = TransactionsLO.instance.groups
		
		XCTAssert(transaction.count >= groups.count,
		          "The groups count should be <= the total number of transactions")
	}
}
