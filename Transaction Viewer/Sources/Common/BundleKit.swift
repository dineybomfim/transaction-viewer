/*
 *	BundleKit.swift
 *	Transaction Viewer
 *
 *	Created by Diney on 27/01/17.
 *	Copyright 2017 Badoo. All rights reserved.
 */

import UIKit

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Extension -
//
//**********************************************************************************************************

extension Bundle {
	
//**************************************************
// MARK: - Properties
//**************************************************
	
	static var appName: String {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
	}
	
	static var appVersion: String {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
	}
	
	static var appBuild: String {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
	}
	
//**************************************************
// MARK: - Exposed Methods
//**************************************************
	
	class func printAppVersionBuild() {
		print("\(self.appName): v\(self.appVersion) Build: \(self.appBuild)")
	}
	
	class func loadPlist(named: String) -> [[String:Any]]? {
		if let path = Bundle.main.path(forResource: named, ofType: "plist") {
			return NSArray(contentsOfFile: path) as? [[String:Any]]
		}
		
		return nil
	}
}
