/*
 *	CurrencyLogicTests.swift
 *	Transaction Viewer
 *
 *	Created by Diney on 27/01/17.
 *	Copyright 2017 Badoo. All rights reserved.
 */

import XCTest
@testable import Transaction_Viewer

class CurrencyLogicTests : XCTestCase {
	
//**************************************************
// MARK: - Protected Methods
//**************************************************

//**************************************************
// MARK: - Exposed Methods
//**************************************************
	
	func testCurrencyLogic_WithLoadingAllPlistContent_ShouldLoadMoreThan0() {
		let transaction = CurrencyLO.instance.all
		
		XCTAssert(transaction.count > 0, "You're testing an empty transaction file")
	}
	
	func testCurrencyLogic_WithConvertionWithSingleTransformation_ShouldSucceed() {
		let logic = CurrencyLO.instance
		
		if let rateModel = logic.all.first {
			let value = 1234.12
			let shouldBe = value * rateModel.rate
			let finalValue = logic.convertCurrency(from: rateModel.from, to: rateModel.to, amount: value)
			
			XCTAssert(finalValue == shouldBe, "The convertion is not correct. It's \(finalValue) and should be \(shouldBe)")
		} else {
			XCTAssert(false, "No possible rates were found in the file")
		}
	}
	
	func testCurrencyLogic_WithStaticFromUSDtoAUD_ShouldConvertValuePassingThroughGBP() {
		let logic = CurrencyLO.instance
		let value = 1234.12
		let shouldBe = value * 0.77 * 0.83
		let finalValue = logic.convertCurrency(from: "USD", to: "AUD", amount: value)
		
		XCTAssert(finalValue == shouldBe, "The convertion is not correct. It's \(finalValue) and should be \(shouldBe)")
	}
}
