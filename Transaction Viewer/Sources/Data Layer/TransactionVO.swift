/*
 *	TransactionVO.swift
 *	Transaction Viewer
 *
 *	Created by Diney on 27/01/17.
 *	Copyright 2017 Badoo. All rights reserved.
 */

import UIKit

//**********************************************************************************************************
//
// MARK: - Type -
//
//**********************************************************************************************************

public struct TransactionVO {

	public var sku: String = ""
	public var currency: String = ""
	public var amount: Double = 0.0
}

//**********************************************************************************************************
//
// MARK: - Extension -
//
//**********************************************************************************************************

extension TransactionVO {
	public init(_ raw: [String:Any]) {
		self.sku = raw["sku"] as? String ?? ""
		self.currency = raw["currency"] as? String ?? ""
		self.amount = Double(raw["amount"] as? String ?? "") ?? 0.0
	}
}
